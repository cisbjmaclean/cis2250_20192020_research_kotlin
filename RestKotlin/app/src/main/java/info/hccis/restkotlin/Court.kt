package info.hccis.restkotlin

data class Court (var courtId: Int, var courtNumber: Int, var courtName: String, var courtType: Int) {
    override fun toString(): String {
        return "Court Name: $courtName"
    }
}