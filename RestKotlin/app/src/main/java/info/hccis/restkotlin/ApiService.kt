package info.hccis.restkotlin

import retrofit2.http.GET
import retrofit2.Call

interface ApiService {
    @GET("court")
    fun getCourtList(): Call<List<Court>>
}